import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/add-contact">Add contact</NavigationItem>
  </ul>
);


export default NavigationItems;