import React, {Component, Fragment} from 'react';
import Contact from "./Contact/Contact";
import {connect} from "react-redux";
import {deleteContact} from "../../store/actions/actionContact";

import './Contacts.css';

class Contacts extends Component {
  state = {
    contact: false,
  };

  toggle = id => {
    this.setState({contact: id});
  };

  toggleCancel = () => {
    this.setState({contact: false});
  };

  render() {
    return (
      Object.keys(this.props.contacts).map((contact, index) => {
        const oneContact = this.props.contacts[contact];
        return (
          <Fragment key={index}>
            <div className="Contacts" onClick={() => this.toggle(contact)} >
              <img src={oneContact.image} alt="ImageContact"/>
              <p>{oneContact.name}</p>
            </div>
            <Contact
              modalOn={this.toggle}
              modalOff={this.toggleCancel}
              show={this.state.contact === contact}
              contact = {contact}
              image={oneContact.image}
              name={oneContact.name}
              phone={oneContact.phone}
              email={oneContact.email}
              remove={(contact)=>this.props.deleteContact(contact)}
            />
          </Fragment>
        )})
    );
  }
}

const mapDispatchToProps = dispatch =>({
  deleteContact: (contactData) => dispatch(deleteContact(contactData))
});

export default connect(null, mapDispatchToProps)(Contacts);