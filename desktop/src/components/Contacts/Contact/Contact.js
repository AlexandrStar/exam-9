import React from 'react';
import Button from "../../UI/Button/Button";
import Modal from "../../UI/Modal/Modal";

import './Contact.css';

const Contact = props => {
  return (
    <Modal
      show={props.show}
      close={props.modalOff}
    >
      <div className="Contact">
        <button onClick={props.modalOff} className="btnClose">X</button>
        <img src={props.image} alt="ImageContact"/>
        <p>{props.name}</p>
        <p>{props.phone}</p>
        <p>{props.email}</p>
        <Button onClick={props.edit} btnType="Success">Edit</Button>
        <Button onClick={() => props.remove(props.contact)} btnType="Danger">Delete</Button>
      </div>
    </Modal>


  );
};

export default Contact;