import React, {Component} from 'react';
import Spinner from "../../UI/Spinner/Spinner";
import FormContact from "../FormContact/FormContact/FormContact";
import {editContact} from "../../../store/actions/actionContact";
import {connect} from "react-redux";


class Edit extends Component {

  state = {
    name: '',
    phone: '',
    email: '',
    image: '',
  };

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  contactHandler = event => {
    event.preventDefault();

    const contactData = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      image: this.state.image,
    };

    this.props.editContact(contactData, this.props.history);
  };

  toggleContinue = () => {
    this.props.history.push('/');
  };

  render() {
    console.log(this.props.match.params.id);
    let form = <FormContact
                name={this.state.name}
                phone={this.state.phone}
                email={this.state.email}
                image={this.state.image}
                changed={this.valueChanged}
                contactHandler={this.contactHandler}
                toggleContinue={this.toggleContinue}
              />;
    if (!this.state.name) {
      form = <Spinner />
    }
    return (
      <div>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts,
  loading: state.loading,
  ordered: state.ordered,
});

const mapDispatchToProps = dispatch => ({
  editContact: (contactData, history) => dispatch(editContact(contactData, history))
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);