import React from 'react';
import Button from "../../../UI/Button/Button";

import '../FormContacts.css';

const FormContact = props => {
  return (
    <div className="ContactData">
      <form>
        <label>Name</label>
        <input className="Input" type="text" name="name" placeholder="Name new contact"
               value={props.name} onChange={props.changed}
        />
        <label>Phone</label>
        <input className="Input" type="text" name="phone" placeholder="Phone number new contact"
               value={props.phone} onChange={props.changed}
        />
        <label>Email</label>
        <input className="Input" type="email" name="email" placeholder="Email new contact"
               value={props.email} onChange={props.changed}
        />
        <label>Photo</label>
        <input className="Input" type="text" name="image" placeholder="Photo new contact"
               value={props.image} onChange={props.changed}
        />
        <p>Photo preview</p>
        {props.image ? <img src={props.image} alt="ImageContact"/>: null}
        <Button onClick={props.contactHandler} btnType="Success">SAVE</Button>
        <Button onClick={props.toggleContinue} btnType="Success">BACK TO CONTACTS</Button>
      </form>
    </div>
  );
};

export default FormContact;