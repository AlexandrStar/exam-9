import React, {Component} from 'react';
import './FormContacts.css';
import {connect} from "react-redux";
import {addedContact} from "../../../store/actions/actionContact";
import FormContact from "./FormContact/FormContact";

class FormContacts extends Component {

  state = {
    name: '',
    phone: '',
    email: '',
    image: '',
  };

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  contactHandler = event => {
    event.preventDefault();

    const contactData = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      image: this.state.image,
    };

    this.props.addedContact(contactData, this.props.history);
  };

  toggleContinue = () => {
    this.props.history.push('/');
  };

  render() {
    return (
      <FormContact
        name={this.state.name}
        phone={this.state.phone}
        email={this.state.email}
        image={this.state.image}
        changed={this.valueChanged}
        contactHandler={this.contactHandler}
        toggleContinue={this.toggleContinue}
      />
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts,
  loading: state.loading,
  ordered: state.ordered,
});

const mapDispatchToProps = dispatch => ({
  addedContact: (contactData, history) => dispatch(addedContact(contactData, history))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormContacts);