import React from 'react';

import './Logo.css';


const Logo = () => (
  <div className="Logo">
    <h1>Contacts</h1>
  </div>
);


export default Logo;