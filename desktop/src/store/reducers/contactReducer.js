import {
  CONTACT_FAILURE,
  CONTACT_REQUEST,
  CONTACT_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  contacts: {},
  loading: false,
  error: null,
  ordered: false,
};

const contactReducer = (state = initialState, action) => {
  switch (action.type) {
  case CONTACT_REQUEST:
      return {...state, loading: true};
  case CONTACT_SUCCESS:
      return {...state, contacts: action.contacts, loading: false, ordered: true};
  case CONTACT_FAILURE:
      return {...state, loading: false, error: action.error};
  default:
    return state;
  }
};

export default contactReducer;