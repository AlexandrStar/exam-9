import axios from '../../axios-order';
import {CONTACT_FAILURE, CONTACT_REQUEST, CONTACT_SUCCESS} from "./actionTypes";

export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = contacts => ({type: CONTACT_SUCCESS, contacts});
export const contactFailure = error => ({type: CONTACT_FAILURE, error});

export const createContacts = () => {
  return dispatch => {
    dispatch(contactRequest());
    axios.get('/contacts.json').then(
      response => {
        dispatch(contactSuccess(response.data));
      },
      error => dispatch(contactFailure(error))
    );
  }
};

export const addedContact = (contactData, history) => {
  return dispatch => {
    dispatch(contactRequest());
    axios.post('contacts.json', contactData).then(
      response => {
        dispatch(contactSuccess());
        history.push('/');
      },
      error => dispatch(contactFailure(error))
    );
  }
};

export const deleteContact = (contactData) => {
  return dispatch => {
    dispatch(contactRequest());
    axios.delete('contacts/' + contactData + '.json').then(
      response => {
        dispatch(createContacts());
      },
      error => dispatch(contactFailure(error))
    );
  }
};

export const editContact = (contactData) => {
  return dispatch => {
    dispatch(contactRequest());
    axios.put('contacts/' + contactData + '.json').then(
      response => {
        dispatch(createContacts());
      },
      error => dispatch(contactFailure(error))
    );
  }
};