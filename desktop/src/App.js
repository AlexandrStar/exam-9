import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom';
import ContactsBuilder from './containers/ContactsBuilder';
import Layout from "./components/Layout/Layout";

import './App.css';
import FormContacts from "./components/Contacts/FormContact/FormContacts";
import EditContact from "./components/Contacts/EditContact/EditContact";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={ContactsBuilder} />
          <Route path="/add-contact" component={FormContacts} />
          <Route exact path="/:id/edit/" component={EditContact}/>
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
