import React, {Component} from 'react';
// import Modal from "../components/UI/Modal/Modal";
import Spinner from "../components/UI/Spinner/Spinner";
import Contacts from "../components/Contacts/Contacts";
import {createContacts} from "../store/actions/actionContact";
import {connect} from "react-redux";
// import Contact from "../components/Contacts/Contact/Contact";

class ContactsBuilder extends Component {
  // state = {
  //   toggleModal: false,
  //   contact: {},
  // };
  //
  // toggle = () => {
  //   this.setState({toggleModal: true});
  // };
  //
  // toggleCancel = () => {
  //   this.setState({toggleModal: false});
  // };
  //
  // purchaseContinue = () => {
  //   this.props.history.push('/');
  // };

  componentDidMount() {
    this.props.createContacts();
  }

  render() {
    return (
      <div className="Container">
        {/*<Modal*/}
          {/*show={this.state.toggleModal}*/}
          {/*close={this.toggleCancel}*/}
        {/*>*/}
          {/*<Contact*/}
            {/*contact={this.state.contact}*/}
          {/*/>*/}
        {/*</Modal>*/}
        {this.props.loading ? <Spinner />  :
          <Contacts
            contacts={this.props.contacts}
            modalOn={this.toggle}
          />}
      </div>
  )
    ;
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts,
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  createContacts: () => dispatch(createContacts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactsBuilder);