import axios from '../../axios-order';
import {CONTACT_FAILURE, CONTACT_REQUEST, CONTACT_SUCCESS, TOGGLE_MODAL} from "./actionTypes";

export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = contacts => ({type: CONTACT_SUCCESS, contacts});
export const contactFailure = error => ({type: CONTACT_FAILURE, error});
export const toggleModal = () => ({type: TOGGLE_MODAL});

export const createList = () => {
  return dispatch => {
    dispatch(contactRequest());
    axios.get('contacts.json').then(response => {
        dispatch(contactSuccess(response.data));
      },
      error => dispatch(contactFailure(error))
    );
  }
};