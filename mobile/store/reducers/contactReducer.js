import {
  CONTACT_FAILURE,
  CONTACT_REQUEST, CONTACT_SUCCESS, TOGGLE_MODAL,
} from "../actions/actionTypes";

const initialState = {
  contacts: {},
  loading: false,
  error: null,
  ordered: false,
  modalVisible: false,
};

const contactReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONTACT_REQUEST:
      return {...state, loading: true};
    case CONTACT_SUCCESS:
      return {...state, loading: false, contacts: action.contacts};
    case CONTACT_FAILURE:
      return {...state, loading: false, error: action.error};
    case TOGGLE_MODAL:
      return {...state, modalVisible: !state.modalVisible};
    default:
      return state;
  }
};

export default contactReducer;