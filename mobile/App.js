import React from 'react';
import {
  StyleSheet, Text, TextInput, View, Button,
  TouchableOpacity, FlatList, Image, Modal, ScrollView
} from 'react-native';


import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {connect, Provider} from 'react-redux';
import reducerDishes from "./store/reducers/contactReducer";
import {contactRequest, createList, toggleModal} from "./store/actions/actionContacts";


class App extends React.Component {

  state = {
    placeName: '',
    contacts: [],
  };

  componentDidMount() {
    this.props.createList();
  }

  placeNameChangedHandler = text => {
    this.setState({placeName: text});
  };

  placeAddedHandler = () => {

    this.setState({
      places: [
        ...this.state.places,
        {
          key: String(Math.random()),
          place: this.state.placeName
        }
      ]
    });
  };



  render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={{fontSize: 25,}}>Contacts</Text>
        </View>
        <View style={styles.flatContainer}>
          <FlatList
            data={Object.keys(this.props.contacts).map((cont) => (this.props.contacts[cont]))}
            renderItem={({item}) => {
              return <TouchableOpacity
                onPress={this.props.toggleModal}
              >
                <View style={styles.Cart}>
                  <Image
                    source={{uri: item.image}}
                    style={{width: 60, height: 60, marginRight: 10}}
                  />
                  <Text style={{fontSize: 20,}}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            }}
          />
        </View>
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.props.modalVisible}
          onRequestClose={this.props.toggleModal}>
          <View style={{margin: 5, backgroundColor: '#fff'}}>
            {Object.keys(this.props.contacts).map((order, key)=>{
              const contact = this.props.contacts[order];

              return (
                <View  key={key} >
                  <Text>Name {contact.name}</Text>
                  <Image
                    source={{uri: contact.image}}
                    style={{width: 60, height: 60, marginRight: 10}}
                  />
                  <Text>Phone {contact.phone}</Text>
                  <Text>Email {contact.email}</Text>
                </View>
              )
            })}


            <Button title={'Back to list'} onPress={this.props.toggleModal}/>
          </View>
        </Modal>

      </View>
    );
  }
}



const mapStateToProps = state => ({
  modalVisible: state.modalVisible,
  contacts: state.contacts,
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModal()),
  createList: () => dispatch(createList()),
});

const AppConnected = connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 30
  },
  flatContainer: {
    flex: 8,
  },
  Cart: {
    padding: 10,
    marginTop: 10,
    height: 80,
    backgroundColor: '#E6E6FA',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    borderBottomColor: '#4B0082',
    borderBottomWidth: 3,
  },
});


const store = createStore(
  reducerDishes,
  applyMiddleware(thunkMiddleware)
);

const index = () => (
  <Provider store={store}>
    <AppConnected />
  </Provider>
);


export default index;