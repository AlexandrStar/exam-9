import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://exam-alexandrcher.firebaseio.com/'
});

export default instance;